package com.zuitt.example;

//Child class of Animal
    //"extends" keyword is used to inherit the properties and method of the parent class


public class Dog extends Animal{
    //properties
    private String breed;

    //constructor
    public Dog(){
        //to have a direct access with the original constructor(PARENT CLASS);
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    //Getter
    public String getBreed(){
        return this.breed;
    }

    //Setter
    public void setBreed(String breed){
        this.breed = breed;
    }

    public void speak(){
        System.out.println("Woof, Woof");
    }

    public void call(){
        super.call();
        System.out.println("Hi! My name is " + this.getName() + ", I am dog.");
    }
}
