package com.zuitt.example;


//Parent Class
public class Animal {
    //properties
    private String name;
    private String color;

    //Constructor
    public Animal(){

    }

    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }

    public String getName(){
        return this.name;
    }

    public String getColor(){
        return this.color;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setColor(String color){
        this.color = color;
    }

    //Method
    public void call(){
        System.out.println("Hi! My name is: " + this.name);
    }

}
