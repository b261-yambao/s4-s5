package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook(){

    }

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    //Class Array Getter
    public ArrayList<Contact> getContacts(){ return this.contacts; }

    //Class Array Setter
    public void setContacts(Contact contacts){ this.contacts.add(contacts);}

}
